package it.unibo.oop.lab.lambda.ex02;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
    
    	List<String> ssongs = new ArrayList<>() ;
    	this.songs.forEach(s -> ssongs.add(s.songName));
    	
    	return ssongs.stream().sorted((o1,o2)-> o1.compareTo(o2));
    	
    }

    @Override
    public Stream<String> albumNames() {
        return this.albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
    	
        return this.albums.keySet()
        		.stream()
        		.filter(s -> this.albums.get(s).equals(year));
    }

    @Override
    public int countSongs(final String albumName) {
    	
        return this.songs.stream()
        		.filter(s -> s.getAlbumName()
        				.orElse(null)
        				.equals(albumName))
        		.mapToInt(a -> 1).sum();
    }

    @Override
    public int countSongsInNoAlbum() {
    	//Vedere se funziona perchè isPresent() ritorna un Boolean
        return this.songs.stream()
        		.filter(s -> s.getAlbumName().isEmpty())
        		.mapToInt(a -> 1).sum();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
    	return this.songs.stream()
    		.filter(s -> s.getAlbumName().equals(albumName))
    		.mapToDouble(s -> s.getDuration())
    		.average();
    	
    }

    @Override
    public Optional<String> longestSong() {
    	return this.songs.stream()
    			.max(new Comparator<Song>() {
		
					@Override
					public int compare(Song o1, Song o2) {
						if(o1.duration > o2.duration) {
							return 1;
						} else if(o1.getDuration() == o2.getDuration()) {
							return 0;
						}
						return -1;
		    		}
    		
    			}).map(s -> s.getSongName());
    			
    }

    @Override
    public Optional<String> longestAlbum() {
    	Map<String,Integer> map = new HashMap<>();
    	
    	
       this.songs
       		.stream()
       		.map(s -> s.getAlbumName()).
       		forEach(new Consumer<Optional<String>>() {
				@Override
				public void accept(Optional<String> t) {
					if(t.isPresent() && !map.containsKey(t.get())) {
						map.put(t.get(), 1);
						return;
					}
					if(t.isPresent() && map.containsKey(t.get())) {
						map.put(t.get(), map.get(t.get())+1);
						return;
					}
					return;   
		       }
       		});
      
       return map.keySet()
    		   .stream()
    		   .max((o1,o2) -> map.get(o1)-map.get(o2));
    	   
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
